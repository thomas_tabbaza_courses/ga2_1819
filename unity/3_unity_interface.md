Les panels disponibles dans l'interface de Unity peuvent être déplacés et redimensionnés afin de créer le layout le plus adapté à votre utilisation.
Vous pouvez sauvegarder et charger des layouts en cliquant sur le bouton en haut à droite de la fenêtre.  
![Layouts](./imgs/layouts.png)

> Tous les onglets peuvent être ouverts depuis le menu `Window` et être fermés en faisant `clic droit/Close Tab`

## Hierarchy
Le panel Hierarchy contient l'ensemble des scènes ouvertes ainsi que les `GameObject` qui y sont présents.

![Hierarchy](./imgs/hierarchy.png)

> Utilisez le clic droit pour ajouter un nouveau GameObject

> Utilisez le champ de recherche pour recherchez un objet par nom ou en utilisant un filtre (ex: `t:Light` pour rechercher les lumières)

## Scene
Le panel Scene est l'éditeur 3D où vous pourrez modifier votre scène visuellement.

## Inspector

Le panel Inspector permet de modifier les caractéristiques des **Components** du et des **GameObjects** séléctionné(s).

![Inspector](./imgs/inspector.png)

## Project

Contient toutes les ressources que vous avez ajouté à votre projet. Deux modes sont disponibles, une ou deux colonnes, cliquez sur les trois ligne à droite de l'onglet pour modifier le mode.

![ProjectPanelOptions](./imgs/project_panel_options.png)

## Game
Contient l'aperçu de la vue de votre caméra principale.

## Console
Contient les informations fournie par l'éditeur (ex: les erreurs dans vos scripts !).